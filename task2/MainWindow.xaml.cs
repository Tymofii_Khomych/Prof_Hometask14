﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace task2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        System.Timers.Timer timer;
        int sec = 0, min = 0;
        public MainWindow()
        {
            InitializeComponent();
            timer = new System.Timers.Timer();
            timer.Interval = 1000;
            timer.Elapsed += OnTimeEvent;

        }

        private void OnTimeEvent(object? sender, ElapsedEventArgs e)
        {
            

            Dispatcher.Invoke(() =>
            {
                sec += 1;
                if (sec == 60)
                {
                    sec = 0;
                    min += 1;
                }
                if(sec % 3 == 0)
                {
                    textBox1.Foreground = sec % 2 == 0 ? Brushes.Purple : Brushes.Blue; 
                    textBox1.Text = "Data received";
                }
                label.Content = $"{min.ToString().PadLeft(2, '0')} : {sec.ToString().PadLeft(2, '0')}";
            });
        }


        //public void Run() { }
        public async Task StartButton()
        {
            await Task.Delay(2000);
            textBox1.Foreground = Brushes.Green;
            textBox1.Text = "Connection successful";

            timer.Start();
        }

        public async Task StopButton()
        {
            await Task.Delay(2000);
            timer.Stop();
            textBox1.Foreground = Brushes.Red;
            textBox1.Text = "Connection lost";

            sec = 0; min = 0;
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            await StopButton();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            await StartButton();
        }
    }
}
