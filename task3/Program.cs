﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace task3
{
    class DataBase
    {
        public void Operation()
        {
            Thread.Sleep(2000);
            Console.WriteLine("Connection to DB");
        }

        public async Task OperationAsync()
        {
            await Task.Factory.StartNew(Operation);
        }
    }

    class Program
    {
        static void Main()
        {
            Console.OutputEncoding = Encoding.Unicode;
            DataBase db = new DataBase();
            Task task = db.OperationAsync();

            task.ContinueWith(t => Console.WriteLine("\nUsing DB"));
        }
    }
}
